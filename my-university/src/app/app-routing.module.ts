import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoodComponent } from './good/good.component';
import { MalaysiaComponent } from './malaysia/malaysia.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'good'},
  {path: 'good', component: GoodComponent},
  {path: 'malaysia', component:MalaysiaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
